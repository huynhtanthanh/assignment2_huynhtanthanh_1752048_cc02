
public class TestCase {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Knight:\n");
		
		Battle.GROUND = 6;
		
		Knight knight = new Knight(500, 1);
		System.out.println("Weapon: " + knight.getWp() + "\nCombat Score: " + knight.getCombatScore() + "\n");
		
		knight.setWp(0);
		System.out.println("Weapon: " + knight.getWp() + "\nCombat Score: " + knight.getCombatScore() + "\n");
		
		Battle.GROUND = 4;
		System.out.println("Weapon: " + knight.getWp() + "\nCombat Score: " + knight.getCombatScore() + "\n");
		
		System.out.println("Warrior:\n");
		
		Battle.GROUND = 6;
		
		Warrior warrior = new Warrior(600, 1);
		System.out.println("Weapon: " + warrior.getWp() + "\nCombat Score: " + warrior.getCombatScore() + "\n");
		
		warrior.setWp(0);
		System.out.println("Weapon: " + warrior.getWp() + "\nCombat Score: " + warrior.getCombatScore() + "\n");
		
		Battle.GROUND = 3;
		System.out.println("Weapon: " + warrior.getWp() + "\nCombat Score: " + warrior.getCombatScore() + "\n");
		
		System.out.println("Paladin:\n");
		
		Battle.GROUND = 6;
		
		Paladin paladin = new Paladin(300, 0);
		System.out.println("Weapon: " + paladin.getWp() + "\nCombat Score: " + paladin.getCombatScore() + "\n");
		
		paladin.setBaseHp(5);
		System.out.println("Weapon: " + paladin.getWp() + "\nCombat Score: " + paladin.getCombatScore() + "\n");
		
		System.out.println("DeathEater:\n");
		
		DeathEater deatheater = new DeathEater(new Complex(5, 5));
		System.out.println("Mana: " + deatheater.toString() + "\nCombat Score: " + deatheater.getCombatScore() + "\n");
	}

}
