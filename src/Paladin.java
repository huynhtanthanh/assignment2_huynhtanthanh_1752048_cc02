public class Paladin extends Knight {

	public Paladin(int baseHp, int wp) {
		super(baseHp, wp);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double getCombatScore() {
		// TODO Auto-generated method stub
		
		int a = 1, n = 1;
		
		while (n < this.getBaseHp()) {
			int temp = a;
			a = n;
			n += temp;
			
			if (this.getBaseHp() == n) return 1000 + n;
		}
		
		return this.getBaseHp() * 3;
	}
	
}
