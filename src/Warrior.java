public class Warrior extends Fighter {

	public Warrior(int baseHp, int wp) {
		super(baseHp, wp);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double getCombatScore() {
		// TODO Auto-generated method stub
		
		if (Utility.isPrime(Battle.GROUND)) return this.getBaseHp() * 2;
		
		if (this.getWp() == 1) return this.getBaseHp();
		
		return this.getBaseHp() / 10.0;
	}
    
}
